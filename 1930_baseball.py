class Player:
	# constructor: defines object
	def __init__(self, name):
		self.name = name
	def get_name(self):
		return self.name
	def get_bats(self):
		return self.bats
	def get_hits(self):
		return self.hits
	def get_runs(self):
		return self.runs
	def get_batavg(self):
		return float(self.hits)/(self.bats)
	def set_bats(self, bats):
		self.bats = bats
	def set_hits(self, hits):
		self.hits = hits
	def set_runs(self, runs):
		self.runs = runs
	def __eq__(self, other):
		if isinstance (other, Player):
			return self.name == other.name
		return NotImplemented

import sys, os, re
 
if len(sys.argv) < 2:
	sys.exit("Usage: %s filename" % sys.argv[0])
 
filename = sys.argv[1]
 
if not os.path.exists(filename):
	sys.exit("Error: File '%s' not found" % sys.argv[1])

players = []
	#stored objects

f = open(filename)
for line in f:
	regex = re.search(r'(\w*\s\w*) batted (\d*) times with (\d*) hits and (\d*) runs', line.rstrip())
	if regex is not None:
		name = regex.group(1)
		if (Player(name)in players):
			index = players.index(Player(name))
			bats = int(players[index].get_bats())+int(regex.group(2))
			hits = int(players[index].get_hits())+int(regex.group(3))
			runs = int(players[index].get_runs())+int(regex.group(4))
			players[index].set_bats(bats)
			players[index].set_hits(hits)
			players[index].set_runs(runs)
		else:
			player = Player(name)
			player.set_bats(regex.group(2))
			player.set_hits(regex.group(3))
			player.set_runs(regex.group(4))
			players.append(player)

f.close() # free up memory when we're finished with the file


sorted_players = sorted(players, key=lambda player: player.get_batavg(), reverse=True )

for player in sorted_players:
	print player.get_name()+": "+"%.3f"%player.get_batavg()
